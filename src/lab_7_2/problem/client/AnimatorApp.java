package lab_7_2.problem.client;

import lab_7_2.problem.graphics.MainWindow;

public class AnimatorApp {

	public static void main(String[] args) {
		MainWindow mainWindow = new MainWindow("Animator Application Window", 50);
		mainWindow.show();
	}
}
