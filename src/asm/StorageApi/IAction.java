package asm.StorageApi;

import asm.impl2.Klass;

import java.util.HashMap;

/**
 * Created by Steven on 1/27/2016.
 */
public abstract class IAction {
     public abstract void setTargetClass(String className);
     public abstract String getTargetClass();
     public abstract void triggerAction(HashMap<String, Klass> map);

     protected void triggerAction(HashMap<String, Klass> map, IKlassPart part){
          Klass k = map.get(getTargetClass());
          if(k != null)
               k.addKlassPart(part);
          else{
               Klass newK = new Klass(getTargetClass(), 0, -1);
               newK.addKlassPart(part);
               map.put(getTargetClass(), newK);
          }

     }
}
