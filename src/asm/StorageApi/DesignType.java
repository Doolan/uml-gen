package asm.StorageApi;

/**
 * Created by Steven on 1/19/2016.
 */
public interface DesignType extends IKlassPart {
    public String getDesignName();
    public String getDesignColor();

}
