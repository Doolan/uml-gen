package asm.GUI;

import asm.DataObjectVisitors.PropertiesRunner;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Properties;

/**
 * Created by Austin on 2/16/2016.
 */
public class UMLFrame {

    private JFrame frame;
    private JPanel contentPane;
    private JPanel btPanel;
    private JPanel loadingPanel;
    private JButton loadButton;
    private JButton analysisButton;
    private JProgressBar progressBar;
    private JFileChooser chooser;
    private JLabel label;
    private InputStream inputStream;

    public UMLFrame() {
        this.frame = new JFrame("ScrollPane Image Demo");
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.chooser = new JFileChooser("");
        this.chooser.setFileFilter(new FileNameExtensionFilter("properties files", "properties"));
        this.label = new JLabel();
        this.contentPane = (JPanel)this.frame.getContentPane();
        this.contentPane.setPreferredSize(new Dimension(400, 100));
        this.btPanel = new JPanel();
        this.loadingPanel = new JPanel();
        this.loadingPanel.setLayout(new FlowLayout());
        this.btPanel.setLayout(new FlowLayout());
        this.btPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        this.contentPane.add(this.btPanel,BorderLayout.NORTH);
        this.contentPane.add(this.loadingPanel,BorderLayout.SOUTH);

        this.loadingPanel.setComponentOrientation(ComponentOrientation.UNKNOWN);
        this.loadingPanel.add(this.label);

        this.loadButton = new JButton("Load Config");
        this.loadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(chooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
                    File file = chooser.getSelectedFile();
                    label.setText(file.getName());
                    try {
                        inputStream = new FileInputStream(file);
                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
        this.analysisButton = new JButton("Analyse");
        this.analysisButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (inputStream != null) {
                        analysisButton.setEnabled(false);
                        loadButton.setEnabled(false);
                        contentPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                        PropertiesRunner pr = PropertiesFetcher.constructPropertiesRunner(inputStream);
                        inputStream.close();

                        //Move to Main Frame
                        new ScrollPaneDemo(pr);
                    }
                } catch (Exception ee) {
                    System.out.println("Exception: " + ee);
                }
            }
        });
        this.btPanel.add(this.loadButton);
        this.btPanel.add(this.analysisButton);

        this.frame.pack();
        this.frame.setVisible(true);
    }

    private static void createAndShowGUI() {
        new UMLFrame();
    }

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}
