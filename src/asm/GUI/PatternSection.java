package asm.GUI;

import com.sun.xml.internal.bind.v2.runtime.ClassBeanInfoImpl;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Austin on 2/19/2016.
 */
public class PatternSection extends JPanel {

    private JLabel patternLabel;
    private JPanel listPanel;
    private List<JCheckBox> classBoxs;

    public PatternSection(String pattern, String...classes) {
        this.patternLabel = new JLabel(pattern);
        this.listPanel = new JPanel();
        this.classBoxs = new LinkedList<>();

        this.listPanel.setLayout(new FlowLayout());

        for(String e:classes) {
            this.classBoxs.add(new JCheckBox(e, true));
        }
        for(JCheckBox e: classBoxs) {
            this.listPanel.add(e);
        }

        this.add(this.patternLabel,BorderLayout.NORTH);
        this.add(this.listPanel,BorderLayout.SOUTH);
    }
}
