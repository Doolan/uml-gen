package asm.GUI;

import asm.DataObjectVisitors.PropertiesRunner;

import java.awt.*;
import java.io.IOException;

import javax.swing.*;

public class ScrollPaneDemo {
	private JFrame frame;
	private JPanel contentPane;
	private JScrollPane imagePane;
	private JScrollPane treePane;
	private JButton loadButton;
	private JMenuBar menubar;
	private PropertiesRunner pr;

	public ScrollPaneDemo(PropertiesRunner props) throws IOException, InterruptedException {
		this.pr = props;
		this.frame = new JFrame("Design Parser - Result");

		//run properties runner
		props.runExectuion();

		this.setUpMenuBar();

		this.contentPane = (JPanel)this.frame.getContentPane();
		this.contentPane.setPreferredSize(new Dimension(1000, 800));

		this.setUpTreeView();
		this.loadImage();

		this.contentPane.add(treePane,BorderLayout.WEST);
		this.contentPane.add(imagePane,BorderLayout.EAST);

		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.pack();
		this.frame.setVisible(true);
	}

	private void setUpMenuBar() {
		this.menubar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenu helpMenu = new JMenu("Help");

		JMenuItem openItem = new JMenuItem("Open");
		JMenuItem saveItem = new JMenuItem("Save");
		fileMenu.add(openItem);
		fileMenu.add(saveItem);

		JMenuItem aboutItem = new JMenuItem("About");
		helpMenu.add(aboutItem);

		this.menubar.add(fileMenu);
		this.menubar.add(helpMenu);
		this.frame.setJMenuBar(this.menubar);
	}

	private void setUpTreeView() {
		this.treePane = new JScrollPane();
		this.treePane.setBorder(BorderFactory.createLineBorder(Color.black));
		this.treePane.setPreferredSize(new Dimension(300, 800));

		pr.ge
	}

	private void loadImage() {
		// Use JLabel to show the image
		Icon roseIcon = new ImageProxy(pr.getImageOutputPath());
		this.imagePane = new JScrollPane(new JLabel(roseIcon));
		this.imagePane.setPreferredSize(new Dimension(700, 800));

		this.imagePane.revalidate();
		this.imagePane.repaint();

		this.contentPane.revalidate();
		this.contentPane.repaint();
	}
}