package asm.GUI;

import asm.DataObjectVisitors.PropertiesRunner;
import asm.asmVisitor.DesignVisitors.AdapterClassVisitor;
import asm.asmVisitor.DesignVisitors.CompositeClassVisitor;
import asm.asmVisitor.DesignVisitors.DecoratorClassVisitor;
import asm.asmVisitor.DesignVisitors.SingletonClassVisitor;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Austin on 2/19/2016.
 */
public class PropertiesFetcher {

    public static PropertiesRunner constructPropertiesRunner(InputStream inputStream) throws IOException {
        PropertiesRunner propertiesRunner = new PropertiesRunner();

        Properties prop = new Properties();
        prop.load(inputStream);

        //Set Output path and executable information
        propertiesRunner.setImageOutputPath(prop.getProperty("PNGImageOutputFilePath"));
        propertiesRunner.setDotFileOutputPath(prop.getProperty("DotFileOutputPath"));
        propertiesRunner.setDotExePath(prop.getProperty("DotExecutablePath"));

        //Load in classes
        String[] classes = prop.getProperty("Input-Classes").split(",");
        for(String e: classes) {
            propertiesRunner.addClassToVisit(e);
        }


        //Add Custom Class Visitors
        propertiesRunner.addCustomVisitor("SingletonClassVisitor", SingletonClassVisitor::new);
        propertiesRunner.addCustomVisitor("DecoratorClassVisitor", DecoratorClassVisitor::new);
        propertiesRunner.addCustomVisitor("AdapterClassVisitor", AdapterClassVisitor::new);
        propertiesRunner.addCustomVisitor("CompositeClassVisitor", CompositeClassVisitor::new);
        propertiesRunner.markInactiveCustomVisitors("SingletonClassVisitor");//turn of singleton detection
        propertiesRunner.markInactiveCustomVisitors("DecoratorClassVisitor");//turn of decorator detection
        propertiesRunner.markInactiveCustomVisitors("AdapterClassVisitor");//turn of adapter detection
        propertiesRunner.markInactiveCustomVisitors("CompositeClassVisitor");//turn of composite detection

        String[] visitors = prop.getProperty("AdditionalVisitor").split(",");
        for(String vis: visitors) {
            propertiesRunner.markActiveCustomVisitors(vis);
        }

        //Add Custom IActions
        //propertiesRunner.addCustomAction();// Don't have any custom Actions

        //Add Additional VisitType Objects
        //propertiesRunner.addCustomVisitTypes(); //Don't have any custom VisitTypes

        //Add Additional Arrows
        //propertiesRunner.addAdditionalArrows(); // Don't have any custom Arrows

        //Set the UML Output Stream
        propertiesRunner.setOutputStream(new FileOutputStream(prop.getProperty("DotFileOutputPath")));

        propertiesRunner.setImageOutputPath(prop.getProperty("ImageOutPath"));

        return propertiesRunner;
    }
}
