package asm;

/**
 * Created by Steven on 1/27/2016.
 */
public final class FilePaths {
        public static final String PROJECTCLASSES = "./config/project-classes.txt";
        public static final String SINGLETON = "./config/singleton.txt";
        public static final String PIZZZAFM = "./config/pizzafm";
        public static final String CHOCOLATE_FACTORY_SINGLETON = "./config/chocolate_factory";
        public static final String LAB2_1 = "./config/lab2_1";
        public static final String LAB2_1_EXAMPLE = "./config/lab2_1_example";

        public static final String LAB5_1_adapter = "./config/lab5_1";
        //composite pattern
        public static final String LAB7_2_menu = "./config/lab7-2_menu";
        public static final String LAB7_2_problem = "./config/lab_7_2_problem";
        public static final String AWT = "./config/awt";
        public static final String SWING = "./config/swing";

}
