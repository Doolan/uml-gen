package asm.impl;

import asm.StorageApi.IKlassPart;

/**
 * Created by Steven on 1/12/2016.
 */
public interface IAssociation extends IKlassPart {
    String getClassName();
}
