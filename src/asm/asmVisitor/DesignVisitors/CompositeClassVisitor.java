package asm.asmVisitor.DesignVisitors;

import asm.StorageApi.IKlass;
import asm.impl2.Actions.AdapteeAction;
import asm.impl2.Actions.CompositeAction;
import asm.impl2.Actions.TargetAction;
import asm.impl2.DesignParts.AdapterDesign;
import asm.impl2.DesignParts.CompositeComponentDesign;
import asm.impl2.KlassDecorator;
import jdk.internal.org.objectweb.asm.Type;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 * Created by Austin on 2/2/2016.
 */
public class CompositeClassVisitor extends ClassVisitor {
    private IKlass klass;
    private boolean isAbstractFlag = false;
    private boolean hasAddFlag = false;
    private boolean hasRemoveFlag = false;
    private boolean designAdded = false;
    private String className;

    public CompositeClassVisitor(int i, IKlass klass) {
        super(i);
        this.klass = klass;
    }

    public CompositeClassVisitor(int i, ClassVisitor classVisitor, IKlass klass) {
        super(i, classVisitor);
        this.klass = klass;
    }

    private boolean conditionsMet(){
        return isAbstractFlag && hasAddFlag && hasRemoveFlag;
    }

    private void UpdateKlass(){
        if(conditionsMet()  && !designAdded) {
            designAdded = true;
//            System.out.println("success");
            klass.addKlassPart(new CompositeComponentDesign(className));
            klass.addAction(new CompositeAction(klass.getName()));
        }
        else if(!conditionsMet() && designAdded){
            //remove klassPart
        }
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        if((access == (Opcodes.ACC_PUBLIC | Opcodes.ACC_SUPER | Opcodes.ACC_ABSTRACT)) || (interfaces.length >=1)) {
            isAbstractFlag = true;
            className = KlassDecorator.fullStripClean(name);
            System.out.println("abstract");
        }
        super.visit(version, access, name, signature, superName, interfaces);
    }


    @Override
    public FieldVisitor visitField(int access, String name, String desc, String signature, Object value) {
        return super.visitField(access, name, desc, signature, value);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {

        if(name.equals("add")) {
            Type[] argTypes = Type.getArgumentTypes(desc);
            System.out.println(argTypes[0].getInternalName());
            System.out.println(className);
            if(argTypes.length == 1 && KlassDecorator.fullStripClean(argTypes[0].getInternalName()).equals(className)) {
                System.out.println("found add");
                hasAddFlag = true;
            }
        } else if(name.equals("remove")) {
            Type[] argTypes = Type.getArgumentTypes(desc);
            if(argTypes.length == 1 && KlassDecorator.fullStripClean(argTypes[0].getInternalName()).equals(className)) {
                System.out.println("found remove");
                hasRemoveFlag = true;
            }
        }

        UpdateKlass();
        return super.visitMethod(access, name, desc, signature, exceptions);
    }

}
