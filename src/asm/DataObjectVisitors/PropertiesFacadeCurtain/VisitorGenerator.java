package asm.DataObjectVisitors.PropertiesFacadeCurtain;

import asm.DataObjectVisitors.ICreateVisitorMethod;
import asm.asmVisitor.StandardVisitors.ClassDeclarationVisitor;
import asm.asmVisitor.StandardVisitors.ClassFieldVisitor;
import asm.asmVisitor.StandardVisitors.ClassMethodVisitor;
import asm.impl2.Klass;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by Steven on 2/16/2016.
 */
public class VisitorGenerator {
//    private static VisitorGenerator uniqueInstance;
    private HashMap<String, ICreateVisitorMethod> customVisitorGenerator;
    private HashSet<String> customVisitorNames;
    private boolean buildAllVisitors;

    public VisitorGenerator(){
        customVisitorGenerator = new HashMap<>();
        customVisitorNames = new HashSet<>();
        buildAllVisitors = false;
    }

//    public static VisitorGenerator getInstance(){
//        if(uniqueInstance == null){
//            synchronized (VisitorGenerator.class){
//                if(uniqueInstance == null){
//                    uniqueInstance = new VisitorGenerator();
//                }
//            }
//        }
//        return uniqueInstance;
//    }

    public void addCustomVisitorGenerator(String name, ICreateVisitorMethod m){
        customVisitorGenerator.put(name, m);
    }

    public void removeCustomVisitorGenerator(String name){
        customVisitorGenerator.remove(name);
    }

    public void addCustomVisitorInstance(String name){
        customVisitorNames.add(name);
    }

    public void removeCustomVisitorInstance(String name){
        customVisitorNames.remove(name);
    }

    public boolean isBuildAllVisitors() {
        return buildAllVisitors;
    }

    public void setBuildAllVisitors(boolean buildAllVisitors) {
        this.buildAllVisitors = buildAllVisitors;
    }

    public ClassVisitor generateVisitors(int api, Klass klass){
        //generate the standard data object ones
        // make class declaration visitor to get superclass and interfaces
        ClassVisitor visitor = new ClassDeclarationVisitor(Opcodes.ASM5, klass);
        // DECORATE declaration visitor with field visitor
        visitor= new ClassFieldVisitor(Opcodes.ASM5, visitor, klass);
        // DECORATE field visitor with method visitor
        visitor = new ClassMethodVisitor(Opcodes.ASM5, visitor, klass);

        if(buildAllVisitors){
           for(String key : customVisitorGenerator.keySet()){
               visitor = customVisitorGenerator.get(key).execute(api, visitor, klass);
           }
        }
        else {
            for(String key : customVisitorNames){
                visitor = customVisitorGenerator.get(key).execute(api, visitor, klass);
            }
        }
        return visitor;
    }
}
