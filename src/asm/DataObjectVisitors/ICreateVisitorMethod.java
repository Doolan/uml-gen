package asm.DataObjectVisitors;

import asm.StorageApi.IKlass;
import org.objectweb.asm.ClassVisitor;

/**
 * Created by Steven on 2/16/2016.
 */
@FunctionalInterface
public interface ICreateVisitorMethod {
    public ClassVisitor execute(int api, ClassVisitor decorated, IKlass klass);
}
