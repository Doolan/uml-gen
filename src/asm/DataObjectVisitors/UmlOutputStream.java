package asm.DataObjectVisitors;

import Lab4_2.singleton.singleton.classic.Singleton;
import asm.StorageApi.*;
import asm.StorageApi.MethodStorage.IMethodPart;
import asm.StorageApi.MethodStorage.IMethodInternalCall;
import asm.asmVisitor.DesignVisitors.SingletonClassVisitor;
import asm.impl.Argument;
import asm.impl2.Arrow;
import asm.impl2.DesignParts.AdapterDesign;
import asm.impl2.DesignParts.DecoratorDesign;
import asm.impl2.DesignParts.SingletonDesign;
import asm.impl2.StandardDataObjects.Interphace;
import asm.impl2.KlassDecorator;
//import asm.mainRunners.Singleton;
import asm.visitorApi.*;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashSet;

/**
 * Created by Steven on 1/12/2016.
 */
public class UmlOutputStream extends FilterOutputStream {
    private final IVisitor visitor;
    private String className;
    private HashSet<Arrow> arrows;

    /**
     * Depends on Klass being the very base object
     *
     * @param out
     * @throws IOException
     */
    public UmlOutputStream(OutputStream out) throws IOException {
        super(out);
        this.arrows = new HashSet<>();
        this.visitor = new Visitor();
        setupPostVisitSuperKlass();
        setupFieldVisitField();
        setupMethodVisitMethod();
        setupPostVisitInterphase();
        setupNameVistKlass();
        setupMethodVisitKlass();
        setupFieldVisitKlass();
        setupPostVisitKlass();
        setupPostVisitField();
        setupPostVisitMethod();
        setupPostVisitMethodUsedKlass();

        //start design stuff
        setupPostVisitSingletonClass();
        setupPreVisitKlass();
        setupPostVisitAdapterDesign();
        setupPostVisitDecoratorDesign();

        setupNameVisitDesignType();
        setupPreVisitDesignType();
    }

    public HashSet<Arrow> getArrows() {
        return arrows;
    }

    public void addArrow(Arrow a) {
        arrows.add(a);
    }

    public void drawArrows() {
        arrows.forEach(a -> this.write(a.getDotEdgeString()));
    }

    public void write(String m) {
        try {
            super.write(m.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void write(IKlassPart part) {
        ITraverser t = (ITraverser) part;
        t.accept(this.visitor);
    }

    public void addVisitType(VisitType type, Class<?> c, IVisitMethod m) {
        this.visitor.addVisit(type, c, m);
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = KlassDecorator.stripFilePath(className);
    }

    //region Setup Methods
    private void setupPostVisitSuperKlass() {
        this.visitor.addVisit(VisitType.PostVisit, ISuperKlass.class, (ITraverser t) -> {
                    ISuperKlass sk = (ISuperKlass) t;
                    if (KlassDecorator.isDesirableObject(sk.getSuperKlass())) {

                        String superName = KlassDecorator.stripFilePath(sk.getSuperKlass());
                        if (!superName.equals("Object")) {
                            arrows.add(new Arrow("solid", "normal", this.className, superName));
                        }
                    }
                }
        );
    }

    private void setupFieldVisitField() {
        this.visitor.addVisit(VisitType.FieldVisit, IField.class, (ITraverser t) -> {
            IField f = (IField) t;
            String line = String.format("%s %s: %s \\l ", f.getAccessLevel(), f.getFieldName(), KlassDecorator.stripClassPath(f.getFieldType()));
            this.write(line);
        });
    }

    private void setupMethodVisitMethod() {
        this.visitor.addVisit(VisitType.MethodVisit, IMethod.class, (ITraverser t) -> {
            IMethod m = (IMethod) t;
            Argument[] args = m.getArguments();
            StringBuilder returnString = new StringBuilder();
            returnString.append(String.format("%s %s ( ", m.getAccessLevel(), m.getMethodName()));
            if (args.length != 0) {
                returnString.append(String.format("%s : %s", args[0].getName(), KlassDecorator.stripClassPath(args[0].getType().toString())));
            }

            for (int i = 1; i < args.length; i++) {
                returnString.append(String.format(", %s : %s", args[i].getName(), KlassDecorator.stripClassPath(args[i].getType().toString())));
            }
            returnString.append(String.format("): %s \\l ", KlassDecorator.stripClassPath(m.getReturnType())));
            this.write(returnString.toString());
        });
    }

    private void setupPostVisitInterphase() {
        this.visitor.addVisit(VisitType.PostVisit, IInterphace.class, (ITraverser t) -> {
            IInterphace phace = (Interphace) t;

            for (String interphaceName : phace.getInterphase()) {
                if (KlassDecorator.isDesirableObject(interphaceName)) {
                    arrows.add(new Arrow("solid", "empty", this.className, KlassDecorator.stripFilePath(interphaceName)));
                }
            }
        });
    }

    private void setupPreVisitKlass() {
        this.visitor.addVisit(VisitType.PreVisit, IKlass.class, (ITraverser t) -> {
            IKlass k = (IKlass) t;
            String nameString = KlassDecorator.stripFilePath(k.getName());
            this.write(String.format("%s [  ", nameString));
        });
    }


    private void setupNameVistKlass() {
        this.visitor.addVisit(VisitType.NameVisit, IKlass.class, (ITraverser t) -> {
            IKlass k = (IKlass) t;
            String nameString = KlassDecorator.stripFilePath(k.getName());
            this.write(String.format(" \n label = \" { %s", nameString));
        });
    }

    private void setupFieldVisitKlass() {
        this.visitor.addVisit(VisitType.FieldVisit, IKlass.class, (ITraverser t) -> {
            IKlass k = (IKlass) t;
            this.write("|");
        });
    }

    private void setupMethodVisitKlass() {
        this.visitor.addVisit(VisitType.MethodVisit, IKlass.class, (ITraverser t) -> {
            IKlass k = (IKlass) t;
            this.write("|");
        });
    }

    private void setupPostVisitKlass() {
        this.visitor.addVisit(VisitType.PostVisit, IKlass.class, (ITraverser t) -> {
            IKlass k = (IKlass) t;
            this.write(String.format(" \n } \" \n ]"));
        });
    }

    private void setupPostVisitField() {
        this.visitor.addVisit(VisitType.PostVisit, IField.class, (ITraverser t) -> {
            IField f = (IField) t;

            String fieldSignature = f.getfieldSignature();
            // fieldSignature is empty: cat the \\ off field type and add to builder
            if (fieldSignature == null || fieldSignature.trim().equals("")) {
                if (className.trim().equals(""))
                    return;
                fieldSignature = className;

            }

            //type is inside of a collection or outer object. Format of style ///<>
            //String carrotedString = KlassDecorator.stripCollection(fieldSignature);
            //Look for multiple params broken by semi-colon
            String[] strArry = fieldSignature.split("[;,:]");

            for (String str : strArry) {
                if (KlassDecorator.isDesirableObject(str)) {
                    if (KlassDecorator.isDesirableObject(str)) {
                        String s = KlassDecorator.fullStripClean(str);
//                            strBuild.append(String.format("%s -> %s \n", className, s));
                        arrows.add(new Arrow("solid", "vee", className, s));
                    }
                }
            }

//            this.write(strBuild.toString());
        });
    }

    private void setupPostVisitMethod() {
        this.visitor.addVisit(VisitType.PostVisit, IMethod.class, (ITraverser t) -> {
            IMethod m = (IMethod) t;
            HashSet<String> set = new HashSet<String>();
            if (m.getReturnType() != "void")
                set.add(m.getReturnType());

            for (Argument arg : m.getArguments()) {
                set.add(KlassDecorator.stripCollection(arg.getType()));
            }

            for (String str : set) {
                if (KlassDecorator.isDesirableObject(str)) {
                    String s = KlassDecorator.fullStripClean(str);
                    arrows.add(new Arrow("dashed", "vee", className, s));
                }
            }
            for (IMethodPart part : m.getMethodParts()) {
                visitor.postVisit((ITraverser) part);
            }
        });
    }

    private void setupPostVisitMethodUsedKlass() {
        this.visitor.addVisit(VisitType.PostVisit, IMethodInternalCall.class, (ITraverser t) -> {
            IMethodInternalCall m = (IMethodInternalCall) t;
            String s = KlassDecorator.fullStripClean(m.getClassName());
            if (KlassDecorator.isDesirableObject(m.getClassName())) {
                arrows.add(new Arrow("dashed", "vee", className, s));
            }
        });
    }

    //DESIGN PATTERNS ============================================================ DESIGN PATTERNS ==/
    private void setupNameVisitDesignType() {
        this.visitor.addVisit(VisitType.NameVisit, DesignType.class, (ITraverser t) -> {
            DesignType des = (DesignType) t;
            this.write(String.format("\\l\\<\\<%s\\>\\>", des.getDesignName()));
        });
    }

    private void setupPreVisitDesignType() {
        this.visitor.addVisit(VisitType.PreVisit, DesignType.class, (ITraverser t) -> {
            DesignType des = (DesignType) t;
            this.write(String.format("style=\"filled\", \n fillcolor = \"%s\"", des.getDesignColor()));
        });
    }

    private void setupPostVisitSingletonClass() {
        this.visitor.addVisit(VisitType.PostVisit, SingletonDesign.class, (ITraverser t) -> {
            SingletonDesign s = (SingletonDesign) t;
            arrows.add(new Arrow("solid", "normal", this.className, this.className));
        });
    }

    private void setupPostVisitAdapterDesign() {
        this.visitor.addVisit(VisitType.PostVisit, AdapterDesign.class, (ITraverser t) -> {
            AdapterDesign ad = (AdapterDesign) t;
            arrows.add(new Arrow("solid", "normal", this.className, ad.getAdapteeName(), "\"\\<\\<adapts\\>\\>\""));
            arrows.add(new Arrow("solid", "empty", this.className, ad.getTargetName()));
        });
    }

    private void setupPostVisitDecoratorDesign() {
        this.visitor.addVisit(VisitType.PostVisit, DecoratorDesign.class, (ITraverser t) -> {
            DecoratorDesign dec = (DecoratorDesign) t;
            arrows.add(new Arrow("solid", "normal", this.className, KlassDecorator.fullStripClean(dec.getUsesName()), "\\\"\\\\<\\\\<decorates\\\\>\\\\>\\\""));
            // edge format:
            // a -> b [ label="a to b" ];
        });
    }
    //endregion

}
