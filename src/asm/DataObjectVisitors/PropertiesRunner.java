package asm.DataObjectVisitors;

import asm.DataObjectVisitors.PropertiesFacadeCurtain.VisitorGenerator;
import asm.StorageApi.IAction;
import asm.impl2.Arrow;
import asm.impl2.Klass;
import asm.visitorApi.VisitTypeData;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;
//import org.apache.commons.io.IOUtils;
//import sun.misc.IOUtils;
import java.lang.Object;

import java.io.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Properties Runner utilizes both a modified Facade Pattern and a modified Command Pattern.
 * The Goal of the class is to open the runner application to expansion past the initial class.
 *
 * Process Runner will run the Application in the Following order. Stages prefaced with - are LOCKED. Stages prefaced with + can be modified at runtime.
 *
 *  CUSTOMIZE PHASE:
 *  Designate Classes to Visit:
 *  + Designate for the ClassVisitor to be run on
 *      Fully qualified Class Names are required
 *      Class names with invalid paths will be skipped over and will not report back to the user
 *  + Prevent Classes from being visited
 *
 *  Designate ClassVisitors:
 *  - Default Klass Object Building ClassVisitors will always be used. This is closed to modification
 *  + Custom Klass Decorating or Action spawning ClassVisitors can be added/removed from the known list and marked as Active/Inactive
 *
 *  Add Additional IActions:
 *  - Default Actions and Actions created by ClassVisitors cannot be modified
 *  + Additional IActions can be added/removed from the Action List. These Actions will be run after the Default Actions are run.
 *
 *  Add Additional VisitType Objects:
 *  - Default VisitType objects including those set for Design Patterns will always be run.
 *  + Additional VisitType Actions will be added to the Visitor and run during specified VisitType.
 *      Any modification using VisitType should use the PropertiesRunner addCustomVisitTypes method not the UmlOutputStream addVisitType
 *
 * Add Additional Arrows:
 *  - Arrows created VisitType Objects will always be generated
 *  + Additional Arrows can be created and added through AddArrow. These Arrows will be added at the same time as Arrows created by Visit Type
 *      Any modification using ArrowGeneration should use the PropertiesRunner addAdditionalArrows method not the UmlOutputStream addArrowMethod
 *
 * Set Default Output Stream:
 * - Set the default output stream that UmlOutput stream will decorate
 *
 * END OF CUSTOMIZATION
 *
 * Run ClassVisitors:
 * - [Class Visiting Phase] Loops over all of the Designated Classes to Visit
 *      - Runs the ClassVisitors on the designated class
 *      - Generates a Klass Object
 *      - Generates IAction Objects
 * - [Action Phase] Apply Actions
 *     - Applies the IActions added during the KlassVisiting Phase
 *     - Applies Additional IActions after the KlassVisiting Phase's Actions have completed
 * - [Output Phase]
 *      - Creates the UmlOutputStream decorating the default output stream
 *      - Visits all of the Klass Objects with both the default and custom VisitType Objects
 *      - Starts writing to the output buffer
 *      - Generates Arrow Objects to be written to the output buffer during the next phase
 * - [Arrow Phase]
 *      - Combines the Collection of Additional Arrow Objects with the Generated Arrow Objects from the Output Phase
 *      - Writes out the combined Arrow Objects to the output stream
 */
public class PropertiesRunner {
    //Singleton Instance
//    private static PropertiesRunner uniqueInstance;

    //customizable properties
    private HashSet<String> classesToVisit;
    private VisitorGenerator visitorGenerator;
    private HashSet<IAction> customActions;
    private HashSet<VisitTypeData> customVisitTypes;
    private HashSet<Arrow> additionalArrows;
    private OutputStream outputStream; //if null, getter creates one
    private String dotFileHeader;
    private int asmAPI = Opcodes.ASM5;
    private String dotFileOutputPath;
    private String imageOutputPath;
    private String dotExePath;

    //execution properties - WARNING: May not always contain values or be instantiated
    protected HashMap<String, Klass> klassObjects;
    protected HashSet<IAction> klassGeneratedActions;
    protected UmlOutputStream umlOut;
    protected HashSet<Arrow> exectuionArrows;

    public PropertiesRunner() {
        //init customizable properties
        this.classesToVisit = new HashSet<>();
//        this.visitorGenerator = VisitorGenerator.getInstance();
        this.visitorGenerator = new VisitorGenerator();
        this.customActions = new HashSet<>();
        this.customVisitTypes = new HashSet<>();
        this.additionalArrows = new HashSet<>();
        this.dotFileOutputPath = "E:\\Steven\\Documents\\Rose\\UML-Gen\\inputOutput\\output.dot";//default value
        this.imageOutputPath = "E:\\Steven\\Documents\\Rose\\UML-Gen\\inputOutput\\output.png";//default value
        this.dotExePath ="dot";
        this.dotFileHeader = "strict digraph G {\n" +
                "    fontname = \"Bitstream Vera Sans\"\n" +
                "    fontsize = 8\n" +
                "\n" +
                "    node [\n" +
                "    fontname = \"Bitstream Vera Sans\"\n" +
                "    fontsize = 8\n" +
                "    shape = \"record\"\n" +
                "    ]\n" +
                "\n" +
                "    edge [\n" +
                "    fontname = \"Bitstream Vera Sans\"\n" +
                "    fontsize = 8\n" +
                "    ]\n";
    }

//    public static PropertiesRunner getInstance(){
//        if(uniqueInstance == null){
//            synchronized (PropertiesRunner.class){
//                if(uniqueInstance == null){
//                    uniqueInstance = new PropertiesRunner();
//                }
//            }
//        }
//        return uniqueInstance;
//    }

    /* --- CUSTOMIZATION METHODS --- */
    //done
    //region ASM API setting

    /**
     * Get the ASM API code for ASM visitors
     * @return curretn ASM API CODE
     */
    public int getAsmAPI() {
        return asmAPI;
    }
    /**
     * Set the ASM API code for ASM visitors
     * @param asmAPI new ASM API code for ASM visitor
     */
    public void setAsmAPI(int asmAPI) {
        this.asmAPI = asmAPI;
    }

    //endregion

    //done
    //region Classes to Visit

    /**
     * Load in the Classes to Visit from a FileReader
     * The format should be: 'name.java\n'
     * @param reader
     */
    public void loadClassesFromFile(FileReader reader){
        BufferedReader br   = new BufferedReader(reader);
        try {
            for (String className; (className = br.readLine()) != null; ) {
                    classesToVisit.add(className);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("File input did not work");
        }
    }

    /**
     * Loads in the Class to later visit
     * @param fullQualifiedClassPath a Fully Qualified Class Path
     */
    public void addClassToVisit(String fullQualifiedClassPath){
        this.classesToVisit.add(fullQualifiedClassPath);
    }

    /**
     * Loads in a Collection of Fully Qualified Class Paths for to be visted later
     * @param classPathCollection
     */
    public void addClassesToVisit(Collection<String> classPathCollection){
        classPathCollection.forEach(name -> this.addClassToVisit(name));
    }

    public void addClassesFromDirectory(String folderPath){
        File folder = new File(folderPath);
        File[] fileList =  folder.listFiles();
        for(File file : fileList){
            if(file.isFile() && file.getAbsolutePath().endsWith(".java"))
                this.addClassToVisit(file.getAbsolutePath()); //is a java class we are looking for
            else if(file.isDirectory())
                this.addClassesFromDirectory(file.getAbsolutePath()); //is a folder that might contain the java classes we are looking for
        }
    }

    public Collection<String> getClassesToVisit() {
        return classesToVisit;
    }

    /**
     * Removes a class from later visits
     * @param fullyQualifiedClassName
     */
    public void removeClassToVisit(String fullyQualifiedClassName){
        this.classesToVisit.remove(fullyQualifiedClassName);
    }

    //endregion

    //done
    //region ClassVisitors
    /**
     * Add Custom ClassVisitors to the pool of known existing ClassVisitors.
     * This method also marks the new Custom ClassVisitor as Active: see markActiveCustomVisitors for details
     * @param visitorName Name the Visitor can be referred to by
     * @param method The constructor method for the Custom ClassVisitor. This method should fit the ICreateVisitorMethod declaration. If the ClassVisitor Constructor requires different input variables than the Constructor declared in the ICreateVisitorMethods. Method will need to be a lambda expression creating a closure to store any additional features
     */
    public void addCustomVisitor(String visitorName, ICreateVisitorMethod method){
        this.markActiveCustomVisitors(visitorName);
        this.visitorGenerator.addCustomVisitorGenerator(visitorName, method);
    }

    /**
     * Remove Custom ClassVisitors from the pool of known existing ClassVisitors
     * This method also marks the removed Custom ClassVisitor as Inactive: see markInactiveCustomVisitors for details
     * @param visitorName Name the ClassVisitor can be referred to by. This should match the name used in addCustomVisitor Method
     */
    public void removeCustomVisitor(String visitorName){
        this.markInactiveCustomVisitors(visitorName);
        this.visitorGenerator.removeCustomVisitorGenerator(visitorName);
    }

    /**
     *  Mark a Custom Known ClassVisitor as active. This ClassVisitor will need to be added by addCustomVisitor before execution to be used.
     *  WARNING: A Positive value set in setUseAllVisitors will override Active/Inactive ClassVisitor settings and will run all known visitors
     * @param visitorName Name the ClassVisitor can be referred to by. This should match the name used in the addCustomVisitor Method
     */
    public void markActiveCustomVisitors(String visitorName){
        this.visitorGenerator.addCustomVisitorInstance(visitorName);
    }

    /**
     * Mark a Custom Known ClassVisitor as inactive.
     * WARNING: A Positive value set in setUseAllVisitors will override Active/Inactive ClassVisitor settings and will run all known visitors
     * @param visitorName visitorName Name the ClassVisitor can be referred to by. This should match the name used in the addCustomVisitor Method
     */
    public void markInactiveCustomVisitors(String visitorName){
        this.visitorGenerator.removeCustomVisitorInstance(visitorName);
    }

    /**
     * Set a flag to control which Custom Known ClassVisitors are used
     * A Positive value will override Active/Inactive ClassVisitor settings and use all Custom Known ClassVisitors
     * A Negative value will defer to the Active/Inactive ClassVisitor settings and use only Active Custom Known ClassVisitors
     * @param useAll a flag to control which Custom Known ClassVisitors are used. Defaults to False
     */
    public void setUseAllVisitors(boolean useAll){
        this.visitorGenerator.setBuildAllVisitors(useAll);
    }

    /**
     * Generate a decorated ClassVisitor tied to the klass object
     * @param klass Klass object to decorate during ASM process
     * @return a decorated ClassVisitor tied to the klass object
     */
    protected ClassVisitor generateDecoratedVisitor(Klass klass){
        return visitorGenerator.generateVisitors(this.asmAPI, klass);
    }
//endregion

    //done
    //region IActions
    /**
     * Add Custom Additional Actions to be run after default customActions and customActions created by ClassVisitors
     * @param customIAction
     */
    public void addCustomAction(IAction customIAction){
        this.customActions.add(customIAction);
    }
    /**
     * remove Custom Additional Actions to be run after default customActions and customActions created by ClassVisitors
     * @param customIAction
     */
    public void removeCustomAction(IAction customIAction) {
        this.customActions.remove(customIAction);
    }
    //endregion

    //done
    //region VisitType
    /**
     * Add Custom Additional Actions to be run during the specified VisitType
     * @param customVisitType
     */
    public void addCustomVisitTypes(VisitTypeData customVisitType){
        this.customVisitTypes.add(customVisitType);
    }

    /**
     * remove Custom Additional VisitTypes to be run during the specified VisitType
     * @param customVisitType
     */
    public void removeCustomVisitTypes(VisitTypeData customVisitType) {
        this.customVisitTypes.remove(customVisitType);
    }
    //endregion

    //done
    //region Arrows
    /**
     * Add Custom Additional Actions to be merged in with Arrows Created by Visit Type Object
     * @param additionalArrow
     */
    public void addAdditionalArrows(Arrow additionalArrow){
        this.additionalArrows.add(additionalArrow);
    }

    /**
     * remove Custom Additional Actions and prevent them from merging in with Arrows Created by Visit Type Objects
     * @param additionalArrow
     */
    public void removeAdditionalArrows(Arrow additionalArrow) {
        this.additionalArrows.remove(additionalArrow);
    }
    //endregion

    //done
    //region Output Stream
    /**
     * Get the default output stream that UmlOutput stream will decorate
     * If the output stream is null, will generate and return the default base output stream
     * @return
     */
    public OutputStream getOutputStream() throws FileNotFoundException {
        if(outputStream == null)
            outputStream = new FileOutputStream(this.dotFileOutputPath);
        return outputStream;
    }

    /**
     * Set the default output stream that UmlOutput stream will decorate
     * @param outputStream
     */
    public void setOutputStream(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public String getDotFileHeader() {
        return dotFileHeader;
    }

    public void setDotFileHeader(String dotFileHeader) {
        this.dotFileHeader = dotFileHeader;
    }

    //endregion

    //region output path

    public String getDotExePath() {
        return dotExePath;
    }

    public void setDotExePath(String dotExePath) {
        this.dotExePath = dotExePath;
    }

    public String getImageOutputPath() {
        return imageOutputPath;
    }

    public void setImageOutputPath(String imageOutputPath) {
        this.imageOutputPath = imageOutputPath;
    }

    public String getDotFileOutputPath() {
        return dotFileOutputPath;
    }

    public void setDotFileOutputPath(String dotFileOutputPath) {
        this.dotFileOutputPath = dotFileOutputPath;
    }

    //endregion

    /* --- EXECUTION METHODS --- */
    /** Run ClassVisitors:
 * - [Class Visiting Phase] Loops over all of the Designated Classes to Visit
 *      - Runs the ClassVisitors on the designated class
 *      - Generates a Klass Object
 *      - Generates IAction Objects
 * - [Action Phase] Apply Actions
 *     - Applies the IActions added during the KlassVisiting Phase
 *     - Applies Additional IActions after the KlassVisiting Phase's Actions have completed
 * - [Output Stream Phase]
 *      - Creates the UmlOutputStream decorating the default output stream
 *      - Visits all of the Klass Objects with both the default and custom VisitType Objects
 *      - Starts writing to the output buffer
 *      - Generates Arrow Objects to be written to the output buffer during the next phase
 * - [Arrow Phase]
 *      - Combines the Collection of Additional Arrow Objects with the Generated Arrow Objects from the Output Phase
 *      - Writes out the combined Arrow Objects to the output stream
 */

    public void runExectuion() throws IOException, InterruptedException {
        //Execute all of the Execution Phase
        this.resetAndInitExecutionVars();
        this.executeClassVisitingPhase();
        this.executeActionPhase();
        this.executeOutputPhase();
        this.executeArrowPhase();
        this.closeOutputStream();
        this.generateImage();

        this.clearExecutionVars();
    }


    /**
     * Generates a set of Klass Objects (klassObjects)
     * Generates a set of IAction Objects (klassGeneratedActions)
     */
    protected void executeClassVisitingPhase(){
        for(String className: this.classesToVisit) {
            try {
                Klass klass = new Klass();
                ClassReader reader = new ClassReader(className);
                ClassVisitor visitor = this.generateDecoratedVisitor(klass);
                reader.accept(visitor, ClassReader.EXPAND_FRAMES);
                this.klassObjects.put(klass.getName(), klass);
                klass.getActions().forEach(a->this.klassGeneratedActions.add(a));
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Class not found: " + className);
            }
        }
    }

    /**
     * Applies the IActions added during the KlassVisiting Phase
     * Applies Additional IActions after the KlassVisiting Phase's Actions have completed
     */
    protected void executeActionPhase(){
        this.klassGeneratedActions.forEach(a -> a.triggerAction(this.klassObjects));
        this.customActions.forEach(a->a.triggerAction(this.klassObjects));
    }

    /**
     *  - Creates the UmlOutputStream decorating the default output stream
     *      - Visits all of the Klass Objects with both the default and custom VisitType Objects
     *      - Starts writing to the output buffer
     *      - Generates Arrow Objects to be written to the output buffer during the next phase
     * @throws IOException
     */
    protected void executeOutputPhase() throws IOException {
        OutputStream dotOut = this.getOutputStream(); //new FileOutputStream(this.dotFileOutputPath);
        this.umlOut = new UmlOutputStream(dotOut);
        this.umlOut.write(this.dotFileHeader);
        this.customVisitTypes.forEach(vT -> umlOut.addVisitType(vT.getType(), vT.getTargetClass(), vT.getiVisitMethod()));
        this.klassObjects.forEach((name, klass) -> {
            umlOut.setClassName(name);
            umlOut.write(klass);
        });
        this.exectuionArrows = umlOut.getArrows();
    }

    /**
     *  Combines the Collection of Additional Arrow Objects with the Generated Arrow Objects from the Output Phase
     *  Writes out the combined Arrow Objects to the output stream
     */
    protected void executeArrowPhase(){
        //removes duplicate arrow by combining in the same hash set
        this.additionalArrows.forEach(a -> this.exectuionArrows.add(a));
        this.exectuionArrows.forEach(a -> this.umlOut.write(a.getDotEdgeString()));
    }

    private void closeOutputStream() throws IOException {
        this.umlOut.write("}");
        this.umlOut.close();
    }

    private void generateImage() throws IOException, InterruptedException {
        String command = String.format("%s -T png -o %s %s", this.dotExePath, this.imageOutputPath, this.dotFileOutputPath);
        System.out.println(command);

        Process proc = Runtime.getRuntime().exec(command);
        proc.waitFor();
    }

    /**
     * Resets the protected variables used during the execution phase
     */
    private void resetAndInitExecutionVars(){
        this.klassObjects =new HashMap<>();
        this.klassGeneratedActions = new HashSet<>();
        this.exectuionArrows = new HashSet<>();
    }

    /**
     *  Clears all of the Execution Vars
     */
    private void clearExecutionVars(){
        this.klassObjects = null;
        this.klassGeneratedActions = null;
        this.exectuionArrows = null;
//        try {
//            this.umlOut.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        this.umlOut = null;
    }

}
