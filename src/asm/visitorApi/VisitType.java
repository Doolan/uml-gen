package asm.visitorApi;

public enum VisitType {
	PreVisit,
	NameVisit,
	FieldVisit,
	MethodVisit,
	PostVisit
}
