package asm.visitorApi;

/**
 * Created by Steven on 2/19/2016.
 */
public class VisitTypeData {
    private VisitType type;
    private Class<?> targetClass;
    private IVisitMethod iVisitMethod;

    public VisitTypeData(VisitType type, Class<?> targetClass, IVisitMethod iVisitMethod) {
        this.type = type;
        this.targetClass = targetClass;
        this.iVisitMethod = iVisitMethod;
    }

    public VisitType getType() {
        return type;
    }

    public void setType(VisitType type) {
        this.type = type;
    }

    public Class<?> getTargetClass() {
        return targetClass;
    }

    public void setTargetClass(Class<?> targetClass) {
        this.targetClass = targetClass;
    }

    public IVisitMethod getiVisitMethod() {
        return iVisitMethod;
    }

    public void setiVisitMethod(IVisitMethod iVisitMethod) {
        this.iVisitMethod = iVisitMethod;
    }
}
