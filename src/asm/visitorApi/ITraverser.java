package asm.visitorApi;

public interface ITraverser {
	public void accept(IVisitor v);
}
