package asm.mainRunners;

import asm.DataObjectVisitors.ICreateVisitorMethod;
import asm.DataObjectVisitors.PropertiesRunner;
import asm.FilePaths;
import asm.StorageApi.IKlass;
import asm.asmVisitor.DesignVisitors.AdapterClassVisitor;
import asm.asmVisitor.DesignVisitors.CompositeClassVisitor;
import asm.asmVisitor.DesignVisitors.DecoratorClassVisitor;
import asm.asmVisitor.DesignVisitors.SingletonClassVisitor;
import org.objectweb.asm.ClassVisitor;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;

/**
 * Created by Steven on 2/16/2016.
 */
public class PropertiesRunnerMain {
    public static final String OUTPUT_PATH = "inputOutput/output.dot";
    public static final String INPUT_FILE_PATH = FilePaths.CHOCOLATE_FACTORY_SINGLETON;

    public static void main(String[] args) throws IOException, InterruptedException {

        PropertiesRunner propertiesRunner = new PropertiesRunner();

        //set output files
        propertiesRunner.setDotFileOutputPath(OUTPUT_PATH);

        //Set Classes to Visit
        propertiesRunner.loadClassesFromFile(new FileReader(INPUT_FILE_PATH));
        //propertiesRunner.removeClassToVisit(className);

        //Add Custom Class Visitors
        propertiesRunner.addCustomVisitor("SingletonClassVisitor", SingletonClassVisitor::new);
        propertiesRunner.addCustomVisitor("DecoratorClassVisitor", DecoratorClassVisitor::new);
        propertiesRunner.addCustomVisitor("AdapterClassVisitor", AdapterClassVisitor::new);
        propertiesRunner.addCustomVisitor("CompositeClassVisitor", CompositeClassVisitor::new);
        propertiesRunner.markInactiveCustomVisitors("SingletonClassVisitor");//turn of singleton detection
        propertiesRunner.markActiveCustomVisitors("SingletonClassVisitor");//turn back on singleton detection

        //Add Custom IActions
        //propertiesRunner.addCustomAction();// Don't have any custom Actions

        //Add Additional VisitType Objects
        //propertiesRunner.addCustomVisitTypes(); //Don't have any custom VisitTypes

        //Add Additional Arrows
        //propertiesRunner.addAdditionalArrows(); // Don't have any custom Arrows

        //Set the input output
        propertiesRunner.setOutputStream(new FileOutputStream(OUTPUT_PATH));
        propertiesRunner.setDotFileOutputPath("C:\\Users\\Steven\\Documents\\CSSE\\uml-gen\\inputOutput\\output.dot");
        propertiesRunner.setImageOutputPath("C:\\Users\\Steven\\Documents\\CSSE\\uml-gen\\inputOutput\\output.png");
        propertiesRunner.setDotExePath("dot");

        propertiesRunner.runExectuion();

    }
}
