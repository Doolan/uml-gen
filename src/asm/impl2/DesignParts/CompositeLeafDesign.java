package asm.impl2.DesignParts;

import asm.StorageApi.DesignType;
import asm.impl2.KlassDecorator;

/**
 * Created by nygrendr on 2/2/2016.
 */
public class CompositeLeafDesign extends KlassDecorator implements DesignType {
    private final String designName ="leaf";
    private String componentName;

    public CompositeLeafDesign(String componentName) {
        this.componentName = componentName;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    @Override
    public String getDesignName() {
        return designName;
    }

    @Override
    public String getDesignColor() {
        return "yellow";
    }
}
