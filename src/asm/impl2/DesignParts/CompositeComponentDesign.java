package asm.impl2.DesignParts;

import asm.StorageApi.DesignType;
import asm.impl2.KlassDecorator;

/**
 * Created by Austin on 2/2/2016.
 */
public class CompositeComponentDesign extends KlassDecorator implements DesignType {
    private final String designName ="component";

    public CompositeComponentDesign(String componentName) {
    }

    @Override
    public String getDesignName() {
        return designName;
    }

    @Override
    public String getDesignColor() {
        return "yellow";
    }
}
