package asm.impl2.DesignParts;

import asm.StorageApi.DesignType;
import asm.impl2.KlassDecorator;

/**
 * Created by Austin on 1/27/2016.
 */
public class AdapterDesign extends KlassDecorator implements DesignType {
    private String designName = "adapter";
    private String adapteeName;
    private String targetName;

    public AdapterDesign(String adapteeName, String targetName) {
        this.adapteeName = adapteeName;
        this.targetName = targetName;
    }

    @Override
    public String getDesignName() {
        return this.designName;
    }

    @Override
    public String getDesignColor() {
        return "firebrick4";
    }

    public String getAdapteeName() {
        return adapteeName;
    }

    public String getTargetName() {
        return targetName;
    }
}
