package asm.impl2.DesignParts;

import asm.StorageApi.DesignType;
import asm.impl2.KlassDecorator;

/**
 * Created by Steven on 1/19/2016.
 */
public class DecoratorDesign extends KlassDecorator implements DesignType {
    private String designName = "decorator";
    private String usesName = null;

    @Override
    public String getDesignName() {
        return this.designName;
    }

    @Override
    public String getDesignColor() {
        return "chartreuse2";
    }

    public String getUsesName() {
        return usesName;
    }

    public void setUsesName(String usesName) {
        this.usesName = usesName;
    }
}
