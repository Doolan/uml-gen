package asm.impl2;

/**
 * Created by Steven on 2/18/2016.
 */
public class Arrow {
    private String edgeType;
    private String headType;
    private String headClassName;
    private String tailClassName;
    private String label;


    public Arrow(String edgeType, String headType, String headClassName, String tailClassName) {
        this(edgeType, headType, headClassName, tailClassName, "");
    }

    public Arrow(String edgeType, String headType, String headClassName, String tailClassName, String label) {
        if (edgeType == null) {
            throw new NullPointerException("Edge Type cannot be null in this constructor");
        }
        if (headType == null) {
            throw new NullPointerException("Head Type cannot be null in this constructor");
        }
        if (headClassName == null) {
            throw new NullPointerException("Head Class Name cannot be null in this constructor");
        }
        if (tailClassName == null) {
            throw new NullPointerException("Tail Class Name cannot be null in this constructor");
        }
        if (tailClassName == null) {
            throw new NullPointerException("Label cannot be null in this constructor");
        }
        this.edgeType = edgeType;
        this.headType = headType;
        this.headClassName = headClassName;
        this.tailClassName = tailClassName;
        this.label = label;
    }

    public String getDotEdgeString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("\n edge [ \n  style=\"" + edgeType + "\", arrowhead = \"" + headType + "\" \n ] " +
                        "\n %s -> %s ",
                this.headClassName, this.tailClassName));
        if (label != null && !label.equals("")) {
            stringBuilder.append("[ label=" + label + " ]");
        }
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Arrow)) return false;

        Arrow arrow = (Arrow) o;

        if (!headClassName.equals(arrow.headClassName)) return false;
        if (!tailClassName.equals(arrow.tailClassName)) return false;
        if (!edgeType.equals(arrow.edgeType)) return false;
        if (!headType.equals(arrow.headType)) return false;
        return !(label != null ? !label.equals(arrow.label) : arrow.label != null);

    }

    @Override
    public int hashCode() {
        int result = headClassName.hashCode();
        result = 31 * result + tailClassName.hashCode();
        result = 31 * result + edgeType.hashCode();
        result = 31 * result + headType.hashCode();
        result = 31 * result + (label != null ? label.hashCode() : 0);
        return result;
    }
}
