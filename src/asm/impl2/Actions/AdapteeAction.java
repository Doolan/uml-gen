package asm.impl2.Actions;

import asm.StorageApi.IAction;
import asm.impl2.DesignParts.AdapteeDesign;
import asm.impl2.DesignParts.TargetDesign;
import asm.impl2.Klass;

import java.util.HashMap;

/**
 * Created by Steven on 1/27/2016.
 */
public class AdapteeAction extends IAction {
    private String targetClass;

    public AdapteeAction(String targetClass) {
        this.targetClass = targetClass;
    }

    @Override
    public void setTargetClass(String className) {
        this.targetClass = className;
    }

    @Override
    public void triggerAction(HashMap<String, Klass> map) {
        triggerAction(map, new AdapteeDesign());
    }

    @Override
    public String getTargetClass() {
        return targetClass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AdapteeAction)) return false;

        AdapteeAction that = (AdapteeAction) o;

        return targetClass.equals(that.targetClass);

    }

    @Override
    public int hashCode() {
        return targetClass.hashCode();
    }
}