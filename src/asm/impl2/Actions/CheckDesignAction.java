package asm.impl2.Actions;

import asm.StorageApi.DesignType;
import asm.StorageApi.IAction;
import asm.StorageApi.IKlassPart;
import asm.impl2.DesignParts.AdapterDesign;
import asm.impl2.Klass;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Created by nygrendr on 2/19/2016.
 */
public class CheckDesignAction extends IAction{
    private String targetClass;
    private DesignType designType;
    private HashMap<String, Collection<String>> returnmap;
    public CheckDesignAction(String targetclass, DesignType designType) {
        this.targetClass = targetclass;
        this.designType = designType;
    }

    @Override
    public void setTargetClass(String className) {
        this.targetClass = className;
    }

    @Override
    public String getTargetClass() {
        return this.targetClass;
    }

    public HashMap<String, Collection<String>> getReturnmap() {
        return returnmap;
    }

    public void setReturnmap(HashMap<String, Collection<String>> returnmap) {
        this.returnmap = returnmap;
    }

    @Override
    public void triggerAction(HashMap<String, Klass> map) {
//        triggerAction(map, new AdapterDesign(adapteeName, targetName ));
//        Klass k = map.get(targetClass);
//        if(k!=null) {
//            ArrayList<IKlassPart> parts = new ArrayList<>(k.getKlassParts());
//            //if(parts.contains)
//        }
       returnmap = new HashMap<>();

        for(String name: map.keySet()){
            map.get(name).getKlassParts().forEach(p -> {
                if(p instanceof DesignType){
                    DesignType t  = (DesignType) p;
                    this.addToMap(returnmap, t.getDesignName(), name);
                }
            });
        };
    }



    private void addToMap(HashMap<String, Collection<String>> map, String design, String className){
        if(map.containsKey(design)){
            map.get(design).add(className);
        }else{
            Collection<String> col = new ArrayList<>();
            col.add(className);
            map.put(design, col);
        }
    }
}
