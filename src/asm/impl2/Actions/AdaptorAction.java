package asm.impl2.Actions;

import asm.StorageApi.IAction;
import asm.impl2.DesignParts.AdapterDesign;
import asm.impl2.Klass;

import java.util.HashMap;

/**
 * Created by Steven on 1/27/2016.
 */
public class AdaptorAction extends IAction {
    private String targetClass;
    private String adapteeName;
    private String targetName;

    //DEPRECIATED
    public AdaptorAction(String targetClass) {
        this(targetClass, "", "");
    }

    public AdaptorAction(String targetClass, String adapteeName, String targetName) {
        this.targetClass = targetClass;
        this.adapteeName = adapteeName;
        this.targetName = targetName;
    }

    @Override
    public void setTargetClass(String className) {
        this.targetClass = className;
    }

    @Override
    public void triggerAction(HashMap<String, Klass> map) {
        triggerAction(map, new AdapterDesign(adapteeName, targetName ));
    }

    @Override
    public String getTargetClass() {
        return targetClass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AdaptorAction)) return false;

        AdaptorAction that = (AdaptorAction) o;

        return targetClass.equals(that.targetClass);

    }

    @Override
    public int hashCode() {
        return targetClass.hashCode();
    }
}