package asm.impl2.Actions;

import asm.StorageApi.*;
import asm.StorageApi.MethodStorage.IMethodPart;
import asm.impl2.DesignParts.CompositeComponentDesign;
import asm.impl2.DesignParts.CompositeCompositeDesign;
import asm.impl2.DesignParts.CompositeLeafDesign;
import asm.impl2.Klass;
import asm.impl2.KlassDecorator;
import asm.impl2.StandardDataObjects.Interphace;
import asm.impl2.StandardDataObjects.Method;
import asm.impl2.StandardDataObjects.SuperKlass;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Steven on 2/2/2016.
 */
public class CompositeAction extends IAction {
    private String callingClass;
    private boolean flagMethodAdd = false;
    private boolean flagMethodRemove =false;
    private boolean flagSuperType = false;
    private HashMap<String, Klass> map;

    public CompositeAction(String callingClass) {
        this.callingClass = callingClass;
    }

    @Override
    public void setTargetClass(String className) {
        this.callingClass = className;
    }

    @Override
    public String getTargetClass() {
        return callingClass;
    }


    @Override
    public void triggerAction(HashMap<String, Klass> map) {
        this.map = map;
        map.forEach((k,v) -> {
            markClass(v);
        });
    }

    private void markClass(Klass k){
        k.getKlassParts().forEach(p -> {
            if(p instanceof  IMethod){
                checkAdd((IMethod) p);
                checkRemove((IMethod) p);
            }
            else if(p instanceof SuperKlass){
                checkSuperType((SuperKlass) p);
            }
            else if(p instanceof Interphace){
                checkSuperType((Interphace) p);
            }
        });
        if(flagMethodAdd && flagMethodRemove && flagSuperType)
            k.addKlassPart(new CompositeCompositeDesign(this.callingClass));
        else if(!flagMethodAdd && !flagMethodRemove && flagSuperType)
            k.addKlassPart(new CompositeLeafDesign(this.callingClass));
        resetFlags();
    }

    private void resetFlags(){
        flagMethodAdd =false;
        flagMethodRemove = false;
        flagSuperType = false;
    }


    private void checkAdd(IMethod m){
        //check superType arg
        if(m.getMethodName().equals("add"))
            flagMethodAdd = true;
    }
    private void checkRemove(IMethod m){
        //check superType arg
        if(m.getMethodName().equals("remove"))
            flagMethodRemove = true;
    }
    private void checkSuperType(SuperKlass sk){
        checkSuperType(sk.getSuperKlass());
    }
    private void checkSuperType(Interphace sk){
        for (String s : sk.getInterphase()) {
            checkSuperType(s);
        }
    }

    private void checkSuperType(String superName){
        if(KlassDecorator.isDesirableObject(superName)) {
            System.out.println("CHECK SUPER TYPE: " + superName);

            if (superName.equals(this.callingClass)) {
                flagSuperType = true;
            } else {
                if (map.containsKey(superName)) {
                    ArrayList<IKlassPart> superParts = new ArrayList<>(map.get(superName).getKlassParts());
                    for (IKlassPart kp : superParts) {
                        if (kp instanceof ISuperKlass) {
                            checkSuperType((SuperKlass) kp);
                        } else if (kp instanceof IInterphace) {
                            checkSuperType((Interphace) kp);
                        }
                    }
                }
            }
        }
    }
}
