package lab_2_1.problem;

public interface IDecryption {
	public char decrypt(char cipher);
}
