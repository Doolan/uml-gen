package lab_2_1.problem;

public interface IEncryption {
	public char encrypt(char plain);
}
