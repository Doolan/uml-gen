package asm.asmVisitor.DesignVisitors;

import asm.StorageApi.IKlassPart;
import asm.asmVisitor.StandardVisitors.ClassDeclarationVisitor;
import asm.impl2.DesignParts.SingletonDesign;
import asm.impl2.Klass;
import jdk.nashorn.internal.runtime.regexp.joni.constants.OPCode;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;

import javax.imageio.IIOException;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created by Steven on 2/2/2016.
 */
public class SingletonClassVisitorTest {

    @Test
    public void ChocolateBoilerNoGetInstance(){
        Klass klass = runSingletonVisitor("testFiles.singleton.ChocolateBoilerNoGetInstance");
        assertFalse(checkContainsSingleton(klass));
    }
    @Test
    public void testChocolateBoilerSingleton(){
        Klass klass = runSingletonVisitor("testFiles.singleton.ChocolateBoilerSingleton");
        assertTrue(checkContainsSingleton(klass));
    }

    @Test
    public void ChocolateBoilerPublicConstructor(){
        Klass klass = runSingletonVisitor("testFiles.singleton.ChocolateBoilerPublicConstructor");
        assertFalse(checkContainsSingleton(klass));
    }

    private Klass runSingletonVisitor(String className){
        Klass klass = new Klass();
        try {
            ClassReader reader = new ClassReader(className);
            //loads name and stuff
            ClassVisitor decVisitor = new ClassDeclarationVisitor(Opcodes.ASM5, klass);
            //singleton visitor
            ClassVisitor singleton = new SingletonClassVisitor(Opcodes.ASM5,decVisitor, klass);
            reader.accept(singleton, ClassReader.EXPAND_FRAMES);
        }
        catch (IOException e){
            System.out.println("Class Not Found: "+ e);
            assertTrue(false);
        }
        return klass;
    }

    private boolean checkContainsSingleton(Klass klass){
        for(IKlassPart part : klass.getKlassParts()){
            if(part instanceof SingletonDesign)
                return true;
        }
        return false;
    }
}