package asm.asmVisitor.DesignVisitors;

import asm.StorageApi.IAction;
import asm.StorageApi.IKlassPart;
import asm.asmVisitor.StandardVisitors.ClassDeclarationVisitor;
import asm.impl2.Actions.CompositeAction;
import asm.impl2.DesignParts.CompositeComponentDesign;
import asm.impl2.Klass;
import org.junit.Test;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created by Steven on 2/2/2016.
 */
public class CompositeClassVisitorTest {

    //tests interface instead of abstract class
    @Test
    public void TestISprite(){
        Klass klass = runCompositeClassVisitor("testFiles.composite.ISprite");
        assertTrue(checkContainsCompositer(klass));
        assertTrue(checkContainsActions(klass));
    }
    //test abstract not top level
    @Test
    public void TestAbstractSprite(){
        Klass klass = runCompositeClassVisitor("testFiles.composite.AbstractSprite");
        assertFalse(checkContainsCompositer(klass));
        assertFalse(checkContainsActions(klass));
    }

    @Test
    public void TestMenuComponent(){
        Klass klass = runCompositeClassVisitor("testFiles.composite.MenuComponent");
        assertTrue(checkContainsCompositer(klass));
        assertTrue(checkContainsActions(klass));
    }

    @Test
    public void TestMyComponent(){
        Klass klass = runCompositeClassVisitor("testFiles.composite.MyComponent");
        assertTrue(checkContainsCompositer(klass));
        assertTrue(checkContainsActions(klass));
    }

    @Test
    public void TestMyComponentNoAdd(){
        Klass klass = runCompositeClassVisitor("testFiles.composite.MyComponentNoAdd");
        assertFalse(checkContainsCompositer(klass));
        assertFalse(checkContainsActions(klass));
    }

    @Test
    public void TestMyComponentNoRemove(){
        Klass klass = runCompositeClassVisitor("testFiles.composite.MyComponentNoRemove");
        assertFalse(checkContainsCompositer(klass));
        assertFalse(checkContainsActions(klass));
    }


    private Klass runCompositeClassVisitor(String className){
        Klass klass = new Klass();
        try {
            ClassReader reader = new ClassReader(className);
            //loads name and stuff
            ClassVisitor decVisitor = new ClassDeclarationVisitor(Opcodes.ASM5, klass);
            //Composite Class Visitor
            ClassVisitor componentVisitor = new CompositeClassVisitor(Opcodes.ASM5,decVisitor, klass);
            reader.accept(componentVisitor, ClassReader.EXPAND_FRAMES);
        }
        catch (IOException e){
            System.out.println("Class Not Found: "+ e);
            assertTrue(false);
        }
        return klass;
    }

    private boolean checkContainsCompositer(Klass klass){
        for(IKlassPart part : klass.getKlassParts()){
            if(part instanceof CompositeComponentDesign)
                return true;
        }
        return false;
    }

    private boolean checkContainsActions(Klass klass){
        boolean compositeAction = false;
        boolean actionSize = klass.getActions().size() == 1;

        for(IAction action : klass.getActions()){
            if(action instanceof CompositeAction)
                compositeAction = true;
        }
        return compositeAction && actionSize;
    }
}