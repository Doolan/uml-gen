package asm.asmVisitor.DesignVisitors;

import asm.StorageApi.IAction;
import asm.StorageApi.IKlass;
import asm.StorageApi.IKlassPart;
import asm.asmVisitor.StandardVisitors.ClassDeclarationVisitor;
import asm.impl2.Actions.ComponentAction;
import asm.impl2.DesignParts.DecoratorDesign;
import asm.impl2.Klass;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Austin on 1/27/2016.
 */
public class DecoratorClassVisitorTest {

    private IKlass klass;

    @Before
    public void setUp() throws Exception {
        this.klass = new Klass();
    }

    @Test
    public void TestSoyDecorator(){
        Klass klass = runDecoratorVisitor("testFiles.decorator.Soy");
        assertTrue(checkContainsDecorator(klass));
        assertTrue(checkContainsActions(klass));
    }

    @Test
    public void TestMilkDecorator(){
        Klass klass = runDecoratorVisitor("testFiles.decorator.Milk");
        assertTrue(checkContainsDecorator(klass));
        assertTrue(checkContainsActions(klass));
    }

    @Test
    public void TestWhipDecorator(){
        Klass klass = runDecoratorVisitor("testFiles.decorator.Whip");
        assertTrue(checkContainsDecorator(klass));
        assertTrue(checkContainsActions(klass));
    }

    @Test
    public void TestMochaDecorator(){
        Klass klass = runDecoratorVisitor("testFiles.decorator.Mocha");
        assertTrue(checkContainsDecorator(klass));
        assertTrue(checkContainsActions(klass));
    }

    @Test
    public void TestSpilledMilkDecorator(){
        Klass klass = runDecoratorVisitor("testFiles.decorator.SpilledMilk");
        assertFalse(checkContainsDecorator(klass));
        assertFalse(checkContainsActions(klass));
    }

    @Test
    public void TestSourWhipDecorator(){
        Klass klass = runDecoratorVisitor("testFiles.decorator.SourWhip");
        assertFalse(checkContainsDecorator(klass));
        assertFalse(checkContainsActions(klass));
    }

    private Klass runDecoratorVisitor(String className){
        Klass klass = new Klass();
        try {
            ClassReader reader = new ClassReader(className);
            //loads name and stuff
            ClassVisitor decVisitor = new ClassDeclarationVisitor(Opcodes.ASM5, klass);
            //singleton visitor
            ClassVisitor decoratorVisitor = new DecoratorClassVisitor(Opcodes.ASM5,decVisitor, klass);
            reader.accept(decoratorVisitor, ClassReader.EXPAND_FRAMES);
        }
        catch (IOException e){
            System.out.println("Class Not Found: "+ e);
            assertTrue(false);
        }
        return klass;
    }

    private boolean checkContainsDecorator(Klass klass){
        for(IKlassPart part : klass.getKlassParts()){
            if(part instanceof DecoratorDesign)
                return true;
        }
        return false;
    }

    private boolean checkContainsActions(Klass klass){
        boolean componentAction = false;
        boolean actionSize = klass.getActions().size() == 1;

        for(IAction action : klass.getActions()){
            if(action instanceof ComponentAction)
                componentAction = true;
        }
        return componentAction && actionSize;
    }


    //-------- OLD TESTS ------------------------------------------//
    @Test
    public void testVisitFalse() throws Exception {
        boolean test = false;
        ClassReader reader = new ClassReader("Lab5_1.problem.client.App");


        // make class declaration visitor to get superclass and interfaces
        DecoratorClassVisitor visitor = new DecoratorClassVisitor(Opcodes.ASM5, klass);
        reader.accept(visitor, ClassReader.EXPAND_FRAMES);

        Field field = Klass.class.getDeclaredField("klassParts");
        field.setAccessible(true);
        for(IKlassPart k: ((Collection<IKlassPart>)field.get(klass))) {
            if(k instanceof DecoratorDesign)
                test = true;
        }
        assertTrue(test == false);
    }

    @Test
    public void testVisitTrue() throws Exception {
        boolean test = false;
        ClassReader reader = new ClassReader("lab_2_1_example.decorator.starbuzz.Espresso");


        // make class declaration visitor to get superclass and interfaces
        DecoratorClassVisitor visitor = new DecoratorClassVisitor(Opcodes.ASM5, klass);
        reader.accept(visitor, ClassReader.EXPAND_FRAMES);


        Field field = Klass.class.getDeclaredField("klassParts");
        field.setAccessible(true);

        ArrayList<IKlassPart> mockKlassParts = new ArrayList<>();
        mockKlassParts.add(new DecoratorDesign());

        field.set(klass, mockKlassParts);
        for(IKlassPart k: ((Collection<IKlassPart>)field.get(klass))) {
            System.out.println(k);

            if(k instanceof DecoratorDesign)
                test = true;
        }
        assertTrue(test);
    }

    @Test
    public void testVisitAndListSuperclasses() throws Exception {
        ClassReader reader = new ClassReader("lab_2_1_example.decorator.starbuzz.Espresso");

        // make class declaration visitor to get superclass and interfaces
        DecoratorClassVisitor visitor = new DecoratorClassVisitor(Opcodes.ASM5, klass);
        reader.accept(visitor, ClassReader.EXPAND_FRAMES);

        Method method = DecoratorClassVisitor.class.getDeclaredMethod("visitAndListSuperclasses", String.class);
        method.setAccessible(true);
        ArrayList<String> superClasses = (ArrayList<String>) method.invoke(visitor, "lab_2_1_example.decorator.starbuzz.Espresso");
        assertTrue(superClasses!= null && superClasses.size()>0);
    }
}