package asm.asmVisitor.DesignVisitors;

import asm.StorageApi.IAction;
import asm.StorageApi.IKlassPart;
import asm.asmVisitor.StandardVisitors.ClassDeclarationVisitor;
import asm.impl2.Actions.AdapteeAction;
import asm.impl2.Actions.TargetAction;
import asm.impl2.DesignParts.AdapterDesign;
import asm.impl2.Klass;
import org.junit.Test;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;
import java.io.IOException;


import static org.junit.Assert.*;

/**
 * Created by Austin on 1/27/2016.
 */
public class AdapterClassVisitorTest {
    @Test
    public void TestIteratorToEnumerationAdapterMissingField(){
        Klass klass = runAdapterVisitor("testFiles.adapter.IteratorToEnumerationAdapterMissingField");
        assertFalse(checkContainsAdaptor(klass));
    }


    @Test
    public void TestIteratorToEnumerationIncorrectConstructor(){
        Klass klass = runAdapterVisitor("testFiles.adapter.IteratorToEnumerationIncorrectConstructor");
        assertFalse(checkContainsAdaptor(klass));
    }

    @Test
    public void TestIteratorToEnumerationAdapter(){
        Klass klass = runAdapterVisitor("testFiles.adapter.IteratorToEnumerationAdapter");
        assertTrue(checkContainsAdaptor(klass));
        assertTrue(checkContainsActions(klass));
    }

    private Klass runAdapterVisitor(String className){
        Klass klass = new Klass();
        try {
            ClassReader reader = new ClassReader(className);
            //loads name and stuff
            ClassVisitor decVisitor = new ClassDeclarationVisitor(Opcodes.ASM5, klass);
            //singleton visitor
            ClassVisitor adaptorVisitor = new AdapterClassVisitor(Opcodes.ASM5,decVisitor, klass);
            reader.accept(adaptorVisitor, ClassReader.EXPAND_FRAMES);
        }
        catch (IOException e){
            System.out.println("Class Not Found: "+ e);
            assertTrue(false);
        }
        return klass;
    }

    private boolean checkContainsAdaptor(Klass klass){
        for(IKlassPart part : klass.getKlassParts()){
            if(part instanceof AdapterDesign)
                return true;
        }
        return false;
    }

    private boolean checkContainsActions(Klass klass){
        boolean actionSize = false;
        boolean adapteeAction = false;
        boolean targetAction = false;
        actionSize = klass.getActions().size() == 2;

        for(IAction action : klass.getActions()){
            if(action instanceof AdapteeAction)
                adapteeAction = true;
            if(action instanceof TargetAction)
                targetAction = true;
        }
        return adapteeAction && targetAction && actionSize;
    }
}