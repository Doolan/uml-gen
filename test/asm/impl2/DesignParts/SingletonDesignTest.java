package asm.impl2.DesignParts;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Austin on 1/27/2016.
 */
public class SingletonDesignTest {

    @Test
    public void testGetDesignName() throws Exception {
        assertEquals("singleton", new SingletonDesign().getDesignName());
    }

    @Test
    public void testGetDesignColor() throws Exception {
        assertEquals("cyan", new SingletonDesign().getDesignColor());
    }
}