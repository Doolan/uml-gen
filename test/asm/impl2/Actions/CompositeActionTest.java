package asm.impl2.Actions;

import asm.StorageApi.IAction;
import asm.StorageApi.IKlassPart;
import asm.impl.Argument;
import asm.impl2.DesignParts.CompositeCompositeDesign;
import asm.impl2.Klass;
import asm.impl2.StandardDataObjects.Method;
import asm.impl2.StandardDataObjects.SuperKlass;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by nygrendr on 2/3/2016.
 */
public class CompositeActionTest {
    HashMap<String, Klass> map;
    Field CompositePartsField;
    private final String MY_COMPONENT_PATH = "mock.classes.MyComponent";
    private final String MY_COMPOSITE_PATH = "asm.StorageApi.MyComposite";
    private final String MY_LEAF_A_PATH = "asm.visitorApi.LeafA";
    private final String MY_LEAF_B_PATH = "asm.visitorApi.LeafB";
    private final Argument[] args = {new Argument("e", MY_COMPONENT_PATH)};
    private final Argument[] badArgs = {new Argument("e", "theWrongClass")};

    @Before
    public void setUp() throws Exception {
        map = new HashMap<>();
        map.put(MY_COMPONENT_PATH, new Klass(MY_COMPONENT_PATH, 0, -1));
        map.put(MY_COMPOSITE_PATH, new Klass(MY_COMPOSITE_PATH, 0, -1));
        map.put(MY_LEAF_A_PATH, new Klass(MY_LEAF_A_PATH, 0, -1));
        map.put(MY_LEAF_B_PATH, new Klass(MY_LEAF_B_PATH, 0, -1));

        CompositePartsField = Klass.class.getDeclaredField("klassParts");
        CompositePartsField.setAccessible(true);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testSetAndGetCallingClass() throws Exception {
        CompositeAction action = new CompositeAction("IField");
        assertEquals(action.getTargetClass(), "IField");
        action.setTargetClass("IMethod");
        assertEquals(action.getTargetClass(), "IMethod");
    }

    @Test
    public void testTriggerActionFoundLeaf() throws Exception {
        Klass myComponent = map.get(MY_COMPONENT_PATH);
        HashSet<IKlassPart> myParts = (HashSet<IKlassPart>) CompositePartsField.get(myComponent);
        myParts.add(new SuperKlass(MY_COMPONENT_PATH));
        CompositePartsField.set(myComponent, myParts);
        Collection<IKlassPart> parts = (Collection<IKlassPart>) CompositePartsField.get(myComponent);
        assertEquals(1, parts.size());

        IAction action = new CompositeAction(MY_COMPONENT_PATH);
        action.triggerAction(map);
        assertEquals(2, parts.size());
    }

    @Test
    public void testTriggerActionNotFoundLeaf() throws Exception {
        Klass myComponent = map.get(MY_COMPONENT_PATH);
        HashSet<IKlassPart> myParts = (HashSet<IKlassPart>) CompositePartsField.get(myComponent);
//        myParts.add(new SuperKlass(MY_COMPONENT_PATH));
        myParts.add(new Method(1, "add", "void", args, null, ""));
        CompositePartsField.set(myComponent, myParts);
        Collection<IKlassPart> parts = (Collection<IKlassPart>) CompositePartsField.get(myComponent);

        assertEquals(1, parts.size());

        IAction action = new CompositeAction(MY_COMPONENT_PATH);
        action.triggerAction(map);
        assertEquals(1, parts.size());
    }

    @Test
    public void testTriggerActionFoundComposite() throws Exception {
        Klass myComponent = map.get(MY_COMPONENT_PATH);
        HashSet<IKlassPart> myParts = (HashSet<IKlassPart>) CompositePartsField.get(myComponent);
        myParts.add(new SuperKlass(MY_COMPONENT_PATH));
        myParts.add(new Method(1, "add", "void", args, null, ""));
        myParts.add(new Method(1, "remove", "void", args, null, ""));
        CompositePartsField.set(myComponent, myParts);
        Collection<IKlassPart> parts = (Collection<IKlassPart>) CompositePartsField.get(myComponent);
        assertEquals(3, parts.size());

        IAction action = new CompositeAction(MY_COMPONENT_PATH);
        action.triggerAction(map);
        assertEquals(4, parts.size());
    }

    @Test
    public void testTriggerActionNotFoundComposite() throws Exception {
        Klass myComponent = map.get(MY_COMPONENT_PATH);
        HashSet<IKlassPart> myParts = (HashSet<IKlassPart>) CompositePartsField.get(myComponent);
        myParts.add(new SuperKlass(MY_COMPONENT_PATH));
        myParts.add(new Method(1, "remove", "void", args, null, ""));
        CompositePartsField.set(myComponent, myParts);
        Collection<IKlassPart> parts = (Collection<IKlassPart>) CompositePartsField.get(myComponent);
        assertEquals(2, parts.size());

        IAction action = new CompositeAction(MY_COMPONENT_PATH);
        action.triggerAction(map);

        for(IKlassPart kp : parts)
            assertTrue(!(kp instanceof CompositeCompositeDesign));
    }

    @Test
    public void testTriggerActionNotFoundComposite2() throws Exception {
        Klass myComponent = map.get(MY_COMPONENT_PATH);
        HashSet<IKlassPart> myParts = (HashSet<IKlassPart>) CompositePartsField.get(myComponent);
        myParts.add(new SuperKlass(MY_COMPONENT_PATH));



        myParts.add(new Method(1, "add", "void", args, null, ""));
        myParts.add(new Method(1, "remove", "void", badArgs, null, ""));
        CompositePartsField.set(myComponent, myParts);
        Collection<IKlassPart> parts = (Collection<IKlassPart>) CompositePartsField.get(myComponent);
        assertEquals(3, parts.size());

        IAction action = new CompositeAction(MY_COMPONENT_PATH);
        action.triggerAction(map);
        System.out.println(parts);

        for(IKlassPart kp : parts)
            assertTrue(!(kp instanceof CompositeCompositeDesign));
    }
}
