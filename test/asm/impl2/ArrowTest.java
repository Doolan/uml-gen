package asm.impl2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by nygrendr on 2/18/2016.
 */
public class ArrowTest {
    @Test
    public void testGetDotEdgeStringSimple() throws Exception {
        String headString = "HEAD_CLASS";
        String tailString = "TAIL_CLASS";
        Arrow arrow = new Arrow("solid", "normal", headString, tailString);
        String dotEdge = arrow.getDotEdgeString();
        String expectedString = String.format("\n edge [ \n  style=\"solid\", arrowhead = \"normal\" \n ] \n %s -> %s \n",
                                    headString, tailString);

        assertEquals(expectedString,dotEdge);
    }

    @Test
    public void testGetDotEdgeStringWithLabel() throws Exception {
        String edgeTypeString = "solid";
        String headTypeString = "normal";
        String headClassString = "HEAD_CLASS";
        String tailClassString = "TAIL_CLASS";
        String labelString = "LABEL";
        Arrow arrow = new Arrow(edgeTypeString, headTypeString, headClassString, tailClassString, labelString);
        String dotEdge = arrow.getDotEdgeString();
        String expectedString = String.format("\n edge [ \n  style=\"solid\", arrowhead = \"normal\" \n ] \n %s -> %s [ label="+labelString+" ]\n",
                headClassString, tailClassString);

        assertEquals(expectedString,dotEdge);
    }

    @Test
    public void testGetDotEdgeStringWithEmptyLabelInConstructor() throws Exception {
        String edgeTypeString = "solid";
        String headTypeString = "normal";
        String headClassString = "HEAD_CLASS";
        String tailClassString = "TAIL_CLASS";
        String labelString = "";
        Arrow arrow = new Arrow(edgeTypeString, headTypeString, headClassString, tailClassString, labelString);
        String dotEdge = arrow.getDotEdgeString();
        String expectedString = String.format("\n edge [ \n  style=\"solid\", arrowhead = \"normal\" \n ] \n %s -> %s \n",
                headClassString, tailClassString);

        assertEquals(expectedString,dotEdge);
    }

    @Test(expected = NullPointerException.class)
    public void testGetDotEdgeStringWithNullEdgeType() throws Exception {
        String edgeTypeString = null;
        String headTypeString = "normal";
        String headClassString = "HEAD_CLASS";
        String tailClassString = "TAIL_CLASS";
        String labelString = "LABEL";
        Arrow arrow = new Arrow(edgeTypeString, headTypeString, headClassString, tailClassString, labelString);
    }

    @Test(expected = NullPointerException.class)
    public void testGetDotEdgeStringWithNullHeadType() throws Exception {
        String edgeTypeString = "solid";
        String headTypeString = null;
        String headClassString = "HEAD_CLASS";
        String tailClassString = "TAIL_CLASS";
        String labelString = "LABEL";
        Arrow arrow = new Arrow(edgeTypeString, headTypeString, headClassString, tailClassString, labelString);
    }

    @Test(expected = NullPointerException.class)
    public void testGetDotEdgeStringWithNullHeadClass() throws Exception {
        String edgeTypeString = "solid";
        String headTypeString = "normal";
        String headClassString = null;
        String tailClassString = "TAIL_CLASS";
        String labelString = "LABEL";
        Arrow arrow = new Arrow(edgeTypeString, headTypeString, headClassString, tailClassString, labelString);
    }

    @Test(expected = NullPointerException.class)
    public void testGetDotEdgeStringWithNullTailClass() throws Exception {
        String edgeTypeString = "solid";
        String headTypeString = "normal";
        String headClassString = "HEAD_CLASS";
        String tailClassString = null;
        String labelString = "LABEL";
        Arrow arrow = new Arrow(edgeTypeString, headTypeString, headClassString, tailClassString, labelString);
    }

    @Test
    public void testGetDotEdgeStringWithNullLabel() throws Exception {
        String edgeTypeString = "solid";
        String headTypeString = "normal";
        String headClassString = "HEAD_CLASS";
        String tailClassString = "TAIL_CLASS";
        String labelString = null;
        Arrow arrow = new Arrow(edgeTypeString, headTypeString, headClassString, tailClassString, labelString);
    }

}
