package testFiles.singleton;

/**
 * Created by Steven on 2/2/2016.
 */
public class ChocolateBoilerNoGetInstance {
    private static ChocolateBoilerNoGetInstance INSTANCE;

    private ChocolateBoilerNoGetInstance() {
    }

//    public static ChocolateBoilerNoGetInstance getInstance() {
//        if (INSTANCE == null) {
//            synchronized (ChocolateBoilerNoGetInstance.class) {
//                if (INSTANCE == null) {
//                    INSTANCE = new ChocolateBoilerNoGetInstance();
//                }
//            }
//        }
//        return INSTANCE;
//    }
}
