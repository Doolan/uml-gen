package testFiles.singleton;

/**
 * Created by Steven on 2/2/2016.
 */
public class ChocolateBoilerPublicConstructor {
    private static ChocolateBoilerPublicConstructor INSTANCE;

    public ChocolateBoilerPublicConstructor() {
    }

    public static ChocolateBoilerPublicConstructor getInstance() {
        if (INSTANCE == null) {
            synchronized (ChocolateBoilerPublicConstructor.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ChocolateBoilerPublicConstructor();
                }
            }
        }
        return INSTANCE;
    }
}
