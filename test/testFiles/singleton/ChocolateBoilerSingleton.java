package testFiles.singleton;

/**
 * Created by Steven on 2/2/2016.
 */
public class ChocolateBoilerSingleton {
    private static ChocolateBoilerSingleton INSTANCE;

    private ChocolateBoilerSingleton() {
    }

    public static ChocolateBoilerSingleton getInstance() {
        if (INSTANCE == null) {
            synchronized (ChocolateBoilerSingleton.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ChocolateBoilerSingleton();
                }
            }
        }
        return INSTANCE;
    }
}
