package testFiles.decorator;

import lab_2_1_example.decorator.starbuzz.Beverage;
import lab_2_1_example.decorator.starbuzz.CondimentDecorator;

public class SourWhip{//} extends CondimentDecorator {
	Beverage beverage;

	public SourWhip(Beverage beverage) {
		this.beverage = beverage;
	}
 
	public String getDescription() {
		return beverage.getDescription() + ", Whip";
	}
 
	public double cost() {
		return .10 + beverage.cost();
	}
}
