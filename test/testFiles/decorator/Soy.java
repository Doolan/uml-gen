package testFiles.decorator;

import lab_2_1_example.decorator.starbuzz.Beverage;
import lab_2_1_example.decorator.starbuzz.CondimentDecorator;

public class Soy extends CondimentDecorator {
	Beverage beverage;

	public Soy(Beverage beverage) {
		this.beverage = beverage;
	}

	public String getDescription() {
		return beverage.getDescription() + ", Soy";
	}

	public double cost() {
		return .15 + beverage.cost();
	}
}
