package testFiles.decorator;

import lab_2_1_example.decorator.starbuzz.Beverage;
import lab_2_1_example.decorator.starbuzz.CondimentDecorator;

public class SpilledMilk extends CondimentDecorator {
	//Beverage beverage;

	public SpilledMilk(Beverage beverage) {
		//beverageC = beverage;
	}

	public String getDescription() {
		return ", Milk";
	}

	public double cost() {
		return .10;
	}
}
