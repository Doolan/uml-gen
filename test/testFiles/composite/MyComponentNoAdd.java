package testFiles.composite;

/**
 * Created by Steven on 2/3/2016.
 */
public abstract class MyComponentNoAdd {

    public void remove(MyComponentNoAdd c){
        throw new UnsupportedOperationException();
    }

    public void method1(){
        throw new UnsupportedOperationException();
    }

    public void method2(){
        throw new UnsupportedOperationException();
    }
}
