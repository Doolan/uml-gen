package testFiles.composite;

/**
 * Created by Steven on 2/3/2016.
 */
public abstract class MyComponentNoRemove {

    public void add(MyComponentNoRemove c){
        throw new UnsupportedOperationException();
    }

    public void method1(){
        throw new UnsupportedOperationException();
    }

    public void method2(){
        throw new UnsupportedOperationException();
    }
}
