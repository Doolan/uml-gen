package testFiles.composite;

import java.awt.*;

public interface ISprite extends Iterable<ISprite> {
	public void move(Dimension space);
	public Shape getShape();
	
	public void add(ISprite s);
	public void remove(ISprite s);
	public ISprite getChild(int index);
}
