package testFiles.adapter;

import java.util.Enumeration;
import java.util.Iterator;

public class IteratorToEnumerationAdapterMissingField<E> implements Enumeration<E> {

	public IteratorToEnumerationAdapterMissingField(Iterator<E> itr) {
		//this.itr = itr;
	}

	@Override
	public boolean hasMoreElements() {
		return false; //itr.hasNext();
	}

	@Override
	public E nextElement() {
		return null;//itr.next();
	}
}
