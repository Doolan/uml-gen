# UML-Gen

## Instructions
Most interactions with our API can be handeled through the PropertiesRunner Class. 
##### PropertiesRunner JavaDoc
```java
/**
 * Properties Runner utilizes both a modified Facade Pattern and a modified Command Pattern.
 * The Goal of the class is to open the runner application to expansion past the initial class.
 *
 * Process Runner will run the Application in the Following order. Stages prefaced with - are LOCKED. Stages prefaced with + can be modified at runtime.
 *
 *  CUSTOMIZE PHASE:
 *  Designate Classes to Visit:
 *  + Designate for the ClassVisitor to be run on
 *      Fully qualified Class Names are required
 *      Class names with invalid paths will be skipped over and will not report back to the user
 *
 *  Designate ClassVisitors:
 *  - Default Klass Object Building ClassVisitors will always be used. This is closed to modification
 *  + Custom Klass Decorating or Action spawning ClassVisitors can be added/removed from the known list and marked as Active/Inactive
 *
 *  Add Additional IActions:
 *  - Default Actions and Actions created by ClassVisitors cannot be modified
 *  + Additional IActions can be added/removed from the Action List. These Actions will be run after the Default Actions are run.
 *
 *  Add Additional VisitType Objects:
 *  - Default VisitType objects including those set for Design Patterns will always be run.
 *  + Additional VisitType Actions will be added to the Visitor and run during specified VisitType.
 *      Any modification using ArrowGeneration should use the PropertiesRunner addArrow method not the UmlOutputStream addArrowMethod
 *
 * Add Additional Arrows:
 *  - Arrows created VisitType Objects will always be generated
 *  + Additional Arrows can be created and added through AddArrow. These Arrows will be added at the same time as Arrows created by Visit Type
 *
 * Set Default Output Stream:
 * - Set the default output stream that UmlOutput stream will decorate
 *
 * END OF CUSTOMIZATION
 *
 * Run ClassVisitors:
 * - [Class Visiting Phase] Loops over all of the Designated Classes to Visit
 *      - Runs the ClassVisitors on the designated class
 *      - Generates a Klass Object
 *      - Generates IAction Objects
 * - [Action Phase] Apply Actions
 *     - Applies the IActions added during the KlassVisiting Phase
 *     - Applies Additional IActions after the KlassVisiting Phase's Actions have completed
 * - [Output Phase]
 *      - Creates the UmlOutputStream decorating the default output stream
 *      - Visits all of the Klass Objects with both the default and custom VisitType Objects
 *      - Starts writing to the output buffer
 *      - Generates Arrow Objects to be written to the output buffer during the next phase
 * - [Arrow Phase]
 *      - Combines the Collection of Additional Arrow Objects with the Generated Arrow Objects from the Output Phase
 *      - Writes out the combined Arrow Objects to the output stream
 */
```


####UML
Place, alter or select a .text file in config/.
Go to src/asm/FilePaths.java and add or select a variable with the filepath string
Open up designParser and set INPUT_FILE_PATH equal to FilePaths.<Path var name>
Run DesignParser
A DOT will be generated and output to the file path described in DesignParser.OUTPUT_PATH
 
Enter a terminal:
```bash
    cd ../output_path_folder
    dot -T png -o <Output Image Name> <DOT file name>
```

####Sequence Diagram
Run the main method in the Sequence Main class. This method takes in a fully quality method/class name and a max depth parameter. The resultant Sd file can be found in the input output folder and loaded into sd edit.

## Mileston 6 Design
The overall design for Milestone 6 is the same as in Milestone 5. For this milestone we added an additional ASM DesignVisitor, an additional IAction, and two additional DesignType classes. We also greatly expanded our testing.
![Alt text](images/Project-UML-milestone-6.png "Milestone Five Design")

## Milestone 5 Design
We expanded the design visitor pattern and added an action phase after data object generation but before output. We also added aditional lambda listners for design objects and altered the IDesign object to return a color
![Alt text](images/Project-UML-M5.png "Milestone Five Design")


## Milestone 4 Design
Our application has several small design changes.  IDesignType was added as interface for IKlassParts that reference design traits. 
SingletonDesign was created extending KlassDecorator and implementing DesignType. Additional a new visitor, SingletonClassVisitor was created to decorate Klass objects with the SingletonDesign Object.
![Alt text](images/Milestone-4-UML.png "Milestone Four Design")

## Milestone 3 Design
Our application has several design types.
Our Asm parser uses a lab_2_1 pattern to decorate visitor pattern visitors.

These visitors utilize our internal data storage which is a slight play on lab_2_1 allowing for multiple unique objects and individual object ownership.
This data structure is also traversable.

Finally we one of two lambda style visitors for visit the data structure depending on if we are looking to generate UML diagrams or sequence diagrams.
![Alt text](images/SimpleDesignDiagram.png "Milestone Three Design")

## Pre Milestone 3 Design
Our design is based off of a culmination of two design patterns. We kept the initial visitor pattern from asm and then, using the decorator pattern, we created a storage system to represent the class structure . 

In this pattern interfaces, a superclass, fields, and methods are all added as decorators to a base class object as parses through the java class files. 

We then created a KlassStorage instance to allow each set of interfaces a shared memory system on which to decorate the class. This keeps the instance updating with each call.

Our design will be simple to adjust and is flexible at runtime. The only issues is the shared memory is not completely thread safe, nor do we use thread safe data structures. 

##M2:
Uses and Association Arrows were introduced
![Alt text](images/??.png "Milestone Two Design")



## Who Did What
###M1:
Everyone alternated pair programming on Doolan's Desktop for the design and the first part of the implementation.
######Austin
Started working on the automated testing once there was enough working code to test.
######Doolan and Davis
Built and designed the initial application

 
 

###M2:
Doolan's Desktop was used for the bulk of the coding.
######Doolan and Davis
Doolan took care of the initial method implantation, the RegEx and String parsing.
Davis took care of the base field alterations implementations.
Davis and Doolan read through and attempted to trouble shoot the following ASM 5.0 class documents:
+ MethodVisitor
  + visitLocalVariableAnnotation
  + visitVarInsn
+ ClassVisitor
+ ClassReader
+ CodeReader

######Niccum
Created tests that established the correct behavior of arrow detection code
Visually tested the  Abstract Factory PizzaStore
Updated UML diagrams of our code

###M3:
For milestone 3, our team did a semi-complete redesign of milestone 1 and 2 in order to better utilize design principals and make the application more versatile. 
######Doolan
Doolan started off this milestone by refactoring the base design. 
He removed and added dependants, removed all of the print methods in instances of klassparts and added and additional Lambda Style visitor pattern. 
He also visually tested these changes to make sure it worked.
######Davis 
Davis then took this modified design and added the ability to sub traverse methods by adding an ASM method visitor instance. 
He got the project up to Milestone 2.
######Doolan and Davis
Doolan and Davis then took the code further to address the milestone 3 requirements. 
Doolan worked on writing a constructor function to initiate the limited traversal using the ASM visitors and design structure.
Davis worked on learning SdEdit and the print pattern needed. Doolan and Davis then debugged and integrated the code
######Niccum
Through the milestone Niccum pulled down the changes to the code and implemented automated unit tests. 
He also manually generated diagrams to use as a baseline for the project code. 
Finally he assisted in Debugging attempts and visually tested the outputted diagrams. 


###M4:
For milestone 4, our team made small design changes to milestone 3 code to add design types
####Disclaimer:
This code was not shown to Chandan on demo day due to complications with the asm files. The features were working, the asm issues have been addressed and little to no modifications has been changed to the M4 specific code. 
######Davis, Doolan, and Niccum
Met and white boarded the design
######Davis 
Davis coded the SingletonClassVisitor and implemented the logic to detect if the class is a singleton.

######Doolan
Doolan created the new classes and interfaces. He also wrote the UMLOutputStream lambda listeners for design and singleton IKlassParts. 

######Niccum
Through the milestone Niccum pulled down the changes to the code and implemented automated unit tests. 
He also manually generated diagrams to use as a baseline for the project code. 
Finally he assisted in Debugging attempts and visually tested the outputed diagrams. 


###M5:
For milestone 5, our team expanded the design visitor pattern and added an action phase after data object generation but before output
######Davis, Doolan, and Niccum 
Met and white boarded the design
######Davis
Davis built the visitor to check and denote the Decorator pattern. He then wrote a separate function to generate the parent structure of a class.
######Doolan
Doolan refactored code base and main function. Added the action phase and create the action classes. Doolan added lambdas to add named arrows and colors.He also took care of the README.md changes.
######Niccum
Niccum built the visitor to check and denote the Adapter pattern. He also updated and added legacy tests and updated the UML.


###M5
There wasn't a whole lot to do for this milestone. Our design allowed us to add the feature with little changes. 
We pretty much needed three things: 
* New ASM visitor + testing
  * This visitor was exceedingly similar to past ASM Design visitors
* New post ASM IAction + testing
  * This IAction was a bit more complicated than past IActions 
* Back testing on all of the design patterns
  * We didn't really do quality design testing before this milestone

######Davis
Davis created the IAction and implemented testing for that action.

######Doolan
Doolan took care of all of the back testing with design patterns and wrote new tests for our new design patterns. He also handled all README updates

######Niccum
Niccum wrote the ASM visitor and updated the UML